import AppMainMessage from './AppMainMessage.js';

export default {
    components: {
        AppMainMessage
    },
    'data': () => ({
        message: 'Hello, World !'
    }),
    template: `
        <AppMainMessage
            :message="message"
        ></AppMainMessage>
    `
};