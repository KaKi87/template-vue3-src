import AppMain from './AppMain.js';

import style from './App.css' assert { type: 'css' };

export default {
    components: {
        AppMain
    },
    'data': () => ({
        style
    }),
    template: `
        <AppMain>
        </AppMain>
    `
}