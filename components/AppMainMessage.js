import style from './AppMainMessage.css' assert { type: 'css' };

export default {
    props: {
        message: String
    },
    'data': () => ({
        style
    }),
    template: `
        <p class="App__Main__Message">
            {{ message }}
        </p>
    `
};